import React from 'react'
import {buyIceCream} from '../Redux/icecream/icecreamActions'
import {connect}  from 'react-redux'
function IceCreamComponent(props) {
    return (
        <div>
            <h1>IceCream The Yummy Ones  +++ {props.numberOfIceCream} </h1>
            <button onClick={props.buyIceCream}>Buy IceCream</button>
        </div>
    )
}
const mapStateToProps=(state)=>{
     return {
         numberOfIceCream:state.icecream.numberOfIceCream
     }
}

const mapStateToDispatch=(dispatch)=>{
     return {
         buyIceCream:()=>dispatch(buyIceCream())
     }
}
export default connect(mapStateToProps,mapStateToDispatch)(IceCreamComponent)
