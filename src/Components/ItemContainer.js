import React from 'react'

import {connect} from 'react-redux'
function ItemContainer(props) {
    console.log(props)
    return (
        <div>
            <h2>Item is - {props.Item} </h2>
            <h4>Total number of item is </h4>
        </div>
    )
}

const mapStateToProps=(state,ownProps)=>{
    const Item=ownProps.cake ?state.cake.numberOfCakes:state.icecream.numberOfIceCream
    return {
        Item:Item
    }
}


export default connect(mapStateToProps)(ItemContainer)
