import {connect} from 'react-redux'
import React from 'react'
import  {buyCake} from '../Redux/index'

function CakeContainer(props) {
    return (
        <div>
            <h1>Cake Stores  - {props.numberOfCakes}</h1>
            <button onClick={props.buyCake}>Order Cake Now  </button>
        </div>
    )
}

const mapStatetoProps =  state =>{
   
    return {
        numberOfCakes:state.cake.numberOfCakes
    }
}

const mapDispatchToProps=dispatch =>{
    return {
        buyCake:()=>dispatch(buyCake())
    }
}

export default connect(mapStatetoProps,mapDispatchToProps)(CakeContainer)
