import {connect} from 'react-redux'
import React,{useState} from 'react'
import  {buyCake} from '../Redux/index'

function NewCakeContainer(props) {
    const [quantity,setQuantity]=useState(1)
    return (
        <div>
            <h1>Cake Stores  - {props.numberOfCakes}</h1>
         <input type='text' value={quantity} onChange={(e)=>setQuantity(e.target.value)}  />
            <button onClick={()=>props.buyCake(quantity)}>Order Cake Now  </button>
         </div>
    )
}

const mapStatetoProps =  state =>{
   
    return {
        numberOfCakes:state.cake.numberOfCakes
    }
}

const mapDispatchToProps=dispatch =>{
    return {
        buyCake:(quantity)=>dispatch(buyCake(quantity))
    }
}

export default connect(mapStatetoProps,mapDispatchToProps)(NewCakeContainer)
