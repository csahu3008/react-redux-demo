import React,{useEffect} from 'react'
import {connect, useStore,useDispatch,useSelector} from 'react-redux'
import {FetchUsers} from '../Redux/User/userActions'
function UserComponents() {
    const usersData=useSelector(state => { return state.user })
    console.log(usersData)
    const dispatch=useDispatch()
    
    useEffect(()=>{
        dispatch(FetchUsers())
    } 
    ,[])
    return (
        usersData.loading ? (<h2>Loading</h2>):usersData.error ? (<h2>usersData.error</h2>):<h2>
           Users List
            {
               usersData && usersData.users.map(u=><p>{u.name}</p>)
            }
        </h2>
    )
}
// const mapStateToProps=(state)=>{
//      return {
//          usersData:state.user
//      }
// }
// const mapDispatchToProps=(dispatch)=>{
//      return {
//          fetchUsers:()=>dispatch(FetchUsers())
//      }
// }
// export default connect(mapStateToProps,mapDispatchToProps)(UserComponents)
export default UserComponents