import React from 'react'
import {useSelector,useDispatch} from 'react-redux'
import {buyCake} from '../Redux/index'
function HooksContainer() {
    const numberOfCakes= useSelector(state => state.numberOfCakes)
    const dispatch = useDispatch()
    return (
        <div>
            <h2>Number of Cakes { numberOfCakes} </h2>
            <button onClick={()=>dispatch(buyCake()) } > BUY Cakes</button>
        </div>
    )
}  

export default HooksContainer
