import {combineReducers} from 'redux'
import {IceCreamReducer} from './Redux/icecream/icecreamReducer'
import {reducer} from './Redux/cake/reducer'
import {userReducer} from './Redux/User/userReducer'

const rootReducer=combineReducers({
    cake:reducer,
    icecream:IceCreamReducer,
    user:userReducer
}) 

export default rootReducer