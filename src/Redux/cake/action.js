import {BUY_CAKE} from './constants'


export const buyCake=(quantity=1)=>{
    return {
        type:BUY_CAKE,
        info:"tells the status of the cake wheter it was sold or not"
        , payload:quantity
    }
}