import {FETCH_USERS_FAILURE,FETCH_USERS_SUCCESS,FETCH_USERS_REQUEST} from './userTypes'
import axios from 'axios'
export const fetchRequest=()=> {
    return{    
    type:FETCH_USERS_REQUEST,
    }
}

export const fetchSuccess=(users)=> {
    return{    
    type:FETCH_USERS_SUCCESS,
    payload:users
    }
}
export const fetchFailure=(error)=> {
    return{    
    type:FETCH_USERS_FAILURE,
    payload:error
    }
}

export const FetchUsers=()=>{
    return (dispatch)=>{
        dispatch(fetchRequest)
       axios.get('https://jsonplaceholder.typicode.com/users').then(response=>{
           const users=response.data
           dispatch(fetchSuccess(users))
       }).catch(error=>{
           const errMsg=error.message
           dispatch(fetchFailure(errMsg))
       })
    }
}