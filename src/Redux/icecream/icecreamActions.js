import {BUY_ICECREAM} from './icecreamConstants'

export const buyIceCream=()=>{
    return {
        type:BUY_ICECREAM,
        info:"BUY AN ICECREAM"
    }
}