import  {BUY_ICECREAM} from './icecreamConstants'
const initialIceCreamState={
    numberOfIceCream:13
}

export const IceCreamReducer=(state=initialIceCreamState,action)=>{
    switch(action.type){
        case BUY_ICECREAM:
            return {
               ...state, numberOfIceCream:state.numberOfIceCream-1
            }
        default:
            return state
    }
}