import React from 'react';
import CakeContainer from './Components/CakeContainer';
import IceCreamComponent from './Components/IceCreamComponent'
import {Provider} from 'react-redux'
import store from './Redux/cake/Store'
import NewCakeContainer from './Components/NewCakeContainer';
import ItemContainer from './Components/ItemContainer';
import UserComponents from './Components/UserComponents';
function App() {
  return (
    <Provider  store={store}>
    <div className="App">
    {/* <ItemContainer cake/>
    <CakeContainer />
    <IceCreamComponent />
    <NewCakeContainer /> */}
    <UserComponents />
    </div>
    </Provider>
  );
}


export default App;
