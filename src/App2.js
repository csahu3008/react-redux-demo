import React from 'react'
import IceCreamComponent from './Components/IceCreamComponent'
import {Provider} from 'react-redux'
import {store} from '../src/Redux/icecream/icecreamStore'
function App2() {
    return (
        <div>
            <Provider store={store}>
            <IceCreamComponent />
            </Provider>
        </div>
    )

}

export default App2
